const torre_1 = document.getElementById("torre_1");
const torre_2 = document.getElementById("torre_2");
const torre_3 = document.getElementById("torre_3");
const resultP = document.getElementById("resultP");
const rules = document.getElementById("rules");

let resultadoP = document.querySelector(".resultado > p")

let usuario = "Pegou";
let suporteDisco = null;

const discoEscolhido = function (discoPego) {
    let numeroDisco = discoPego.childElementCount;

    if (numeroDisco === 0) {
        resultadoP.innerHTML = '';
        torre_1.style.animation = 'bounce 0.2s 1';
        torre_2.style.animation = 'bounce 0.2s 1';
        torre_3.style.animation = 'bounce 0.2s 1';
        resultP.style.animation = 'colorEffects 0.3s 10';
    } else {
        suporteDisco = discoPego.lastElementChild;
        usuario = "Liberado"
        resultadoP.innerHTML = '';
        torre_1.style.animation = 'bounce 0.2s 1';
        torre_2.style.animation = 'bounce 0.2s 1';
        torre_3.style.animation = 'bounce 0.2s 1';
        resultP.style.animation = 'colorEffects 0.3s 10';
    }
    win();
}

const PegouDiscoDestino = function (evento) {

    if (usuario === "Pegou") {
        discoEscolhido(evento.currentTarget);
    } else if (usuario === "Liberado") {
        torreDistino(evento.currentTarget);
    }
}

const torreDistino = function (discoLiberado) {
    let ultimoElemento = discoLiberado.lastElementChild;

    if (!ultimoElemento) {
        discoLiberado.appendChild(suporteDisco);
        usuario = "Pegou";
        torre_1.style.animation = 'bounce 0.2s 1';
        torre_2.style.animation = 'bounce 0.2s 1';
        torre_3.style.animation = 'bounce 0.2s 1';
        resultP.style.animation = 'colorEffects 0.3s 10';
    } else {
        let discoAtualEscolhido = ultimoElemento.clientWidth;
        let lastPickeddiscoWidth = suporteDisco.clientWidth;

        if (discoAtualEscolhido <= lastPickeddiscoWidth) {
            resultadoP.innerHTML = 'Não pode fazer isso, está nas regras: Um disco maior não pode ser colocado em cima de um disco menor.';
            usuario = "Pegou";
            torre_1.style.animation = 'jogadaProibida 0.3s 2';
            torre_2.style.animation = 'jogadaProibida 0.3s 2';
            torre_3.style.animation = 'jogadaProibida 0.3s 2';
            resultP.style.animation = 'pulsate-fwd 0.3s 6';
        } else {
            discoLiberado.appendChild(suporteDisco);
            usuario = "Pegou"
            resultadoP.innerHTML = '';
            torre_1.style.animation = 'bounce 0.2s 1';
            torre_2.style.animation = 'bounce 0.2s 1';
            torre_3.style.animation = 'bounce 0.2s 1';
            resultP.style.animation = 'colorEffects 0.3s 10';
        }
    }
    win();
}

const win = function () {
    if (torre_1.childElementCount === 0 && torre_3.childElementCount === 4) {
        resultadoP.innerHTML = '<b>Você venceu ! Parabéns !<b>';
        torre_1.style.animation = 'fimDeJogoCartas 0.6s linear both';
        torre_2.style.animation = 'fimDeJogoCartas 0.6s linear 0.3s both';
        torre_3.style.animation = 'fimDeJogoCartas 0.6s linear 0.6s both';
        rules.style.animation = 'fimDeJogoCartasSUP 0.6s linear 0.9s both';
        resultP.style.animation = 'colorEffectsEndGame 2s infinite 1.2s';
    }
}

const torreEventListener = function () {
    torre_1.addEventListener('click', PegouDiscoDestino)
    torre_2.addEventListener('click', PegouDiscoDestino)
    torre_3.addEventListener('click', PegouDiscoDestino)
}
torreEventListener();